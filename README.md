# WhiteBored
A chat application made with [socket.io](http://socket.io/) and [express](https://expressjs.com)

## Install and Usage

From within the WhiteBored folder
`$ npm install`

To start server
`$ npm start`

### Local usage

Open `localhost:3000` in Chrome, use multiple windows or tabs to simulate multiple users.

### Heroku

This application is ready to be deployed via Heroku
